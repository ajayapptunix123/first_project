<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');
// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
// Route::resource('user_data','App\Http\Controllers\UserDataController');



Route::namespace('App\Http\Controllers')->group(function () {

    Route::resource('user', 'UserController');


    Route::prefix('email')->group(function () {
        Route::get('/', 'MailController@email')->name('email');
        Route::post('/send', 'MailController@sendMail')->name('email.send');
    });



});

