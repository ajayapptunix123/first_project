@extends('layouts.admin')
@section('title')
    User Table
@endsection

@section('content')
    <div class="content">
        <div class="card-body">
            <div class="table-responsive">
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('user.create') }}"> Create New Post</a>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-dismissable alert-success ">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            {{ $message }}
                        </strong>
                    </div>
                @endif
                @if (Session::has('no-results'))
                    <span>{{ Session::get('no-results') }}</span>
                @else
                    <table class="table table-hover">
                        <thead class="">
                            <th> S.No. </th>
                            <th> ID </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Phone no </th>
                            <th> Address </th>
                            <th> Action </th>
                        </thead>
                        <tbody>
                            @foreach ($users as $index => $user)
                                <tr>
                                    <td> {{ $index + $users->firstItem() }} </td>
                                    <td> {{ $user->id }} </td>
                                    <td> {{ $user->username }} </td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ $user->phone_no ?? '--' }} </td>
                                    <td> {{ $user->address }} </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('user.show', $user->id) }}">Show</a>

                                        <a class="btn btn-primary" href="{{ route('user.edit', $user->id) }}">Edit</a>
                                        <a href="javascript::void()">
                                            <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                                                <button type="submit" class="btn btn-danger"> Delete</button>
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
    </div>
    @endif
@endsection
