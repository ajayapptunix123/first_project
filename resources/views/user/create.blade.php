@extends('layouts.admin')
@section('title','Add ')
@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card">

            <div class="card-header card-header-primary">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ Route('user.index') }}"> Back</a>
                </div>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                      <strong>Error!</strong> <br>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>

                  </div>
              @endif
              <h4 class="card-title">Add user</h4>
              <p class="card-category">Complete your profile</p>
            </div>
            <div class="card-body">

              <form action="{{ route('user.store') }}" method="POST">
                @csrf
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      <label class="bmd-label-floating">Username</label>
                      <input type="text" class="form-control"  name="username" value="{{ old('username') }}">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="bmd-label-floating">Email</label>
                      <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating"> Phone no.</label>
                      <input type="phone" class="form-control" name="phone_no" value="{{ old('phone_no') }}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Address</label>
                      <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                    </div>
                  </div>
                </div>



                <button type="submit" class="btn btn-primary pull-right">save</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>

    </div>
  </div>
  @endsection
