@extends('layouts.admin')
@section('title','Edit')
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header card-header-primary">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
                        </div>

                        <h4 class="card-title">Details</h4>

                    </div>
                    <div class="card-body">


                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Name:</strong>
                                    {{ $user->username }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    {{ $user->email }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Contact No.:</strong>
                                    {{ $user->phone_no }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Address:</strong>
                                    {{ $user->address }}
                                </div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>

        </div>
    </div>
    @endsection
