<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  border: 45px;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>User Details</h2>

<table>
  <tr>
    <th>Email:</th>
    <td>{{ $email }}</td>
  </tr>
  <tr>
    <th>Address:</th>
    <td>{{ $address }}</td>
  </tr>
  <tr>
    <th>message:</th>
    <td>{{ $sms }}</td>
  </tr>


</table>

</body>
</html>
