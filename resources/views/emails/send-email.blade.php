@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-header card-header-primary">


                            <h4 class="card-title">Edit user</h4>
                            <p class="card-category">Complete your profile</p>
                        </div>
                        <div class="card-body">

                            <form action="{{ route('email.send') }}" method="post">
                                @csrf
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="" placeholder="email">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="address" id="" placeholder="address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <textarea name="sms" id="" cols="30" rows="5"
                                                placeholder="typr here some message"></textarea>

                                            <button type="submit" class="btn btn-primary pull-right">submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
