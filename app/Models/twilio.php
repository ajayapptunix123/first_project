<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class twilio extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'isVerified'
    ];
}
