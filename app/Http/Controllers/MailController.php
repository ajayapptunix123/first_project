<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
// use Barryvdh\DomPDF\PDF;


class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'sms' => 'required',
        ]);
        $data = $request->except('_token');

        $data['pdf'] = PDF::loadView('emails.orders.shipped', $data);

        $fileName = time().'.pdf';

        \Storage::put('public/pdf/'.$fileName, $data['pdf']->output());

        // $data['filepath'] = \URL::to('storage/pdf/'.$fileName);
        $data['name'] = $fileName;

        $data['filepath'] = public_path('storage/pdf/'.$fileName);

        Mail::to($data['email'])->send(new OrderShipped($data));


        // return $pdf->download('laravel.pdf');

        return back();
    }

    public function email()
    {
        return view('emails.send-email');
    }
    public function generatePDF(Request $request)
    {
        $data = [
            'title' => 'Welcome to laravel tutorial ',
            'date' => date('m/d/Y')
        ];

        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('laravel.pdf');
    }

    // public function success()
    // {
    //     return view('email.success');
    // }
}
